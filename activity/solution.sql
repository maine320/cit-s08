SELECT * FROM artists WHERE name LIKE '%d%';
+----+---------------+
| id | name          |
+----+---------------+
|  4 | Lady Gaga     |
|  6 | Ariana Grande |
+----+---------------+


MariaDB [music_db]> SELECT * FROM songs WHERE length < 230;
+----+---------------+----------+-------------------------+----------+
| id | song_name     | length   | genre                   | album_id |
+----+---------------+----------+-------------------------+----------+
|  3 | Pardon Me     | 00:02:23 | Rock                    |        1 |
|  4 | Stellar       | 00:02:00 | Rock                    |        1 |
|  6 | Love Story    | 00:02:13 | Country                 |        3 |
|  8 | Red           | 00:02:04 | Country                 |        4 |
|  9 | Black Eyes    | 00:00:00 | Rock and roll           |        5 |
| 10 | Shallow       | 00:02:01 | Country rock, Folk rock |        5 |
| 12 | Sorry         | 00:01:32 | Dancehall-poptropical   |        7 |
| 15 | Thank U, Next | 00:01:56 | Pop, R&B                |       10 |
| 16 | 24k Magic     | 00:02:07 | Funk, disco, R&B        |       11 |
| 17 | Lost          | 00:01:52 | Pop                     |       12 |
+----+---------------+----------+-------------------------+----------+


MariaDB [music_db]> SELECT albums.album_title, songs.song_name, songs.length FROM albums JOIN songs ON albums.id = songs.album_id;
+-----------------+----------------+----------+
| album_title     | song_name      | length   |
+-----------------+----------------+----------+
| Psy 6           | Gangnam Style  | 00:02:53 |
| Make yourself   | Pardon Me      | 00:02:23 |
| Make yourself   | Stellar        | 00:02:00 |
| Fearless        | Fearless       | 00:02:46 |
| Fearless        | Love Story     | 00:02:13 |
| Red             | State of Grace | 00:02:43 |
| Red             | Red            | 00:02:04 |
| A Star Is Born  | Black Eyes     | 00:00:00 |
| A Star Is Born  | Shallow        | 00:02:01 |
| A Star Is Born  | Born This Way  | 00:02:52 |
| Purpose         | Sorry          | 00:01:32 |
| Believe         | Boyfriend      | 00:02:51 |
| Dangerous Woman | Into You       | 00:02:42 |
| Thank U, Next   | Thank U, Next  | 00:01:56 |
| 24K Magic       | 24k Magic      | 00:02:07 |
| Earth to Mars   | Lost           | 00:01:52 |
+-----------------+----------------+----------+



MariaDB [music_db]> SELECT name, album_title, date_released FROM artists JOIN albums ON artists.id = albums.artist_id WHERE album_title LIKE '%a%';
+---------------+-----------------+---------------+
| name          | album_title     | date_released |
+---------------+-----------------+---------------+
| Incubus       | Make yourself   | 1999-10-26    |
| Taylor Swift  | Fearless        | 2008-11-11    |
| Lady Gaga     | A Star Is Born  | 2018-10-10    |
| Lady Gaga     | Born This Way   | 2011-06-29    |
| Ariana Grande | Dangerous Woman | 2016-05-20    |
| Ariana Grande | Thank U, Next   | 2019-02-08    |
| Bruno Mars    | 24K Magic       | 2018-11-18    |
| Bruno Mars    | Earth to Mars   | 2011-01-20    |
+---------------+-----------------+---------------+


MariaDB [music_db]> SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;
+----+---------------+---------------+-----------+
| id | album_title   | date_released | artist_id |
+----+---------------+---------------+-----------+
| 10 | Thank U, Next | 2019-02-08    |         6 |
|  4 | Red           | 2012-10-22    |         3 |
|  7 | Purpose       | 2015-11-13    |         5 |
|  2 | Psy 6         | 2012-01-15    |         2 |
+----+---------------+---------------+-----------+


MariaDB [music_db]> SELECT album_title, date_released, artist_id, song_name, length, genre FROM albums INNER JOIN songs ON albums.id = songs.album_id ORDER BY album_title DESC, song_name ASC;
+-----------------+---------------+-----------+----------------+----------+------------------------------------+
| album_title     | date_released | artist_id | song_name      | length   | genre                              |
+-----------------+---------------+-----------+----------------+----------+------------------------------------+
| Thank U, Next   | 2019-02-08    |         6 | Thank U, Next  | 00:01:56 | Pop, R&B                           |
| Red             | 2012-10-22    |         3 | Red            | 00:02:04 | Country                            |
| Red             | 2012-10-22    |         3 | State of Grace | 00:02:43 | Rock, alternative rock, arena rock |
| Purpose         | 2015-11-13    |         5 | Sorry          | 00:01:32 | Dancehall-poptropical              |
| Psy 6           | 2012-01-15    |         2 | Gangnam Style  | 00:02:53 | K-Pop                              |
| Make yourself   | 1999-10-26    |         1 | Pardon Me      | 00:02:23 | Rock                               |
| Make yourself   | 1999-10-26    |         1 | Stellar        | 00:02:00 | Rock                               |
| Fearless        | 2008-11-11    |         3 | Fearless       | 00:02:46 | Pop rock                           |
| Fearless        | 2008-11-11    |         3 | Love Story     | 00:02:13 | Country                            |
| Earth to Mars   | 2011-01-20    |         7 | Lost           | 00:01:52 | Pop                                |
| Dangerous Woman | 2016-05-20    |         6 | Into You       | 00:02:42 | EDM house                          |
| Believe         | 2015-06-15    |         5 | Boyfriend      | 00:02:51 | Pop                                |
| A Star Is Born  | 2018-10-10    |         4 | Black Eyes     | 00:00:00 | Rock and roll                      |
| A Star Is Born  | 2018-10-10    |         4 | Born This Way  | 00:02:52 | Electropop                         |
| A Star Is Born  | 2018-10-10    |         4 | Shallow        | 00:02:01 | Country rock, Folk rock            |
| 24K Magic       | 2018-11-18    |         7 | 24k Magic      | 00:02:07 | Funk, disco, R&B                   |
+-----------------+---------------+-----------+----------------+----------+------------------------------------+

